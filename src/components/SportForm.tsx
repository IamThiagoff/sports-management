import { useState, ChangeEvent, FormEvent } from 'react';
import { useRouter } from 'next/router';

type SportFormProps = {
  sport?: Sport;
};

type Sport = {
  id: number;
  name: string;
  description: string;
  image: string;
  numberOfPlayers: string;
  rules: string;
  category: string;
  equipment: string;
  popularity: string;
  origin: string;
  status: string;
};

export default function SportForm({ sport }: SportFormProps) {
  const [form, setForm] = useState<Sport>({
    id: sport?.id || 0,
    name: sport?.name || '',
    description: sport?.description || '',
    image: sport?.image || '',
    numberOfPlayers: sport?.numberOfPlayers || '',
    rules: sport?.rules || '',
    category: sport?.category || '',
    equipment: sport?.equipment || '',
    popularity: sport?.popularity || '',
    origin: sport?.origin || '',
    status: sport?.status || '',
  });

  const router = useRouter();
  const isEdit = !!sport;

  const handleSubmit = async (e: FormEvent) => {
    e.preventDefault();
    const method = isEdit ? 'PUT' : 'POST';
    const url = isEdit ? `/api/sports/${sport?.id}` : '/api/sports';

    await fetch(url, {
      method,
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(form),
    });

    router.push('/sports');
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const { name, value } = e.target;
    setForm({ ...form, [name]: value });
  };

  return (
    <form onSubmit={handleSubmit} className="space-y-4">
      <div>
        <label className="block text-sm font-medium text-gray-700">Nome</label>
        <input
          type="text"
          name="name"
          value={form.name}
          onChange={handleChange}
          className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
        />
      </div>
      {/* Add other fields similarly */}
      <button type="submit" className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600">
        {isEdit ? 'Atualizar' : 'Criar'} Modalidade
      </button>
    </form>
  );
}
