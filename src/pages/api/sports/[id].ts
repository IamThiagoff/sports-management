import { NextApiRequest, NextApiResponse } from 'next';

type Sport = {
  id: number;
  name: string;
  description: string;
  image: string;
  numberOfPlayers: string;
  rules: string;
  category: string;
  equipment: string;
  popularity: string;
  origin: string;
  status: string;
};

let sports: Sport[] = [];

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  const { id } = req.query;
  const sportId = parseInt(id as string);

  if (req.method === 'GET') {
    const sport = sports.find((s) => s.id === sportId);
    if (sport) {
      res.status(200).json(sport);
    } else {
      res.status(404).json({ message: 'Modalidade não encontrada' });
    }
  } else if (req.method === 'PUT') {
    const index = sports.findIndex((s) => s.id === sportId);
    if (index !== -1) {
      sports[index] = { id: sportId, ...req.body };
      res.status(200).json(sports[index]);
    } else {
      res.status(404).json({ message: 'Modalidade não encontrada' });
    }
  } else if (req.method === 'DELETE') {
    const index = sports.findIndex((s) => s.id === sportId);
    if (index !== -1) {
      sports.splice(index, 1);
      res.status(204).end();
    } else {
      res.status(404).json({ message: 'Modalidade não encontrada' });
    }
  } else {
    res.setHeader('Allow', ['GET', 'PUT', 'DELETE']);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
