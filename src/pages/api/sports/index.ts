import { NextApiRequest, NextApiResponse } from 'next';

type Sport = {
  id: number;
  name: string;
  description: string;
  image: string;
  numberOfPlayers: string;
  rules: string;
  category: string;
  equipment: string;
  popularity: string;
  origin: string;
  status: string;
};

let sports: Sport[] = [];

export default function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === 'GET') {
    res.status(200).json(sports);
  } else if (req.method === 'POST') {
    const newSport: Sport = { id: Date.now(), ...req.body };
    sports.push(newSport);
    res.status(201).json(newSport);
  } else {
    res.setHeader('Allow', ['GET', 'POST']);
    res.status(405).end(`Method ${req.method} Not Allowed`);
  }
}
