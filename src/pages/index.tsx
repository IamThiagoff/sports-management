import Link from 'next/link';
import { Inter } from 'next/font/google';

const inter = Inter({ subsets: ['latin'] });

export default function Home() {
  return (
    <div className="container mx-auto p-4">
      <h1 className="text-3xl font-bold mb-6">Plataforma de Gerenciamento de Modalidades Esportivas</h1>
      <div className="flex flex-col space-y-4">
        <Link href="/sports" className="text-blue-500 text-lg hover:underline">
          Gerenciar Modalidades Esportivas
        </Link>
      </div>
    </div>
  );
}
