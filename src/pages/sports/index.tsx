import Link from 'next/link';
import { useState, useEffect } from 'react';

type Sport = {
  id: number;
  name: string;
  description: string;
};

export default function SportsList() {
  const [sports, setSports] = useState<Sport[]>([]);

  useEffect(() => {
    fetch('/api/sports')
      .then((response) => response.json())
      .then((data) => setSports(data));
  }, []);

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Modalidades Esportivas</h1>
      <Link href="/sports/new">
        <span className="text-blue-500 cursor-pointer">Adicionar Nova Modalidade</span>
      </Link>
      <ul className="mt-4">
        {sports.map((sport) => (
          <li key={sport.id} className="mb-2">
            <Link href={`/sports/${sport.id}/edit`}>
              <span className="text-blue-700 cursor-pointer">{sport.name}</span>
            </Link>
            <p>{sport.description}</p>
          </li>
        ))}
      </ul>
    </div>
  );
}
