import SportForm from '@/components/SportForm';

export default function NewSport() {
  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Adicionar Nova Modalidade</h1>
      <SportForm />
    </div>
  );
}
