import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import SportForm from '@/components/SportForm';

type Sport = {
  id: number;
  name: string;
  description: string;
  image: string;
  numberOfPlayers: string;
  rules: string;
  category: string;
  equipment: string;
  popularity: string;
  origin: string;
  status: string;
};

export default function EditSport() {
  const router = useRouter();
  const { id } = router.query;
  const [sport, setSport] = useState<Sport | null>(null);

  useEffect(() => {
    if (id) {
      fetch(`/api/sports/${id}`)
        .then((response) => response.json())
        .then((data) => setSport(data));
    }
  }, [id]);

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Editar Modalidade</h1>
      {sport && <SportForm sport={sport} />}
    </div>
  );
}
